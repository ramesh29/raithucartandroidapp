package com.tractile.smartmerchanterp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.OperationApplicationException;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.RemoteException;
import android.os.StrictMode;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.telephony.PhoneNumberUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.GeolocationPermissions;
import android.webkit.JavascriptInterface;
import android.webkit.URLUtil;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.pos.escposprinter.EscPosPrinter;
import com.pos.escposprinter.connection.DeviceConnection;
import com.pos.escposprinter.connection.usb.UsbConnection;
import com.pos.escposprinter.connection.usb.UsbPrintersConnections;
import com.pos.escposprinter.exceptions.EscPosBarcodeException;
import com.pos.escposprinter.exceptions.EscPosConnectionException;
import com.pos.escposprinter.exceptions.EscPosEncodingException;
import com.pos.escposprinter.exceptions.EscPosParserException;
import com.pos.escposprinter.textparser.PrinterTextParserImg;
import com.pos.thermalprinter.async.AsyncEscPosPrinter;
import com.pos.thermalprinter.async.AsyncUsbEscPosPrint;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import jxl.write.WriteException;

public class MainActivity<Cookie> extends AppCompatActivity implements NetworkChangeReceiver.ConnectionChangeCallback {


    private WebView mywebview = null;
    private ProgressBar progressBar;
    // variables para manejar la subida de archivos
    private final static int FILECHOOSER_RESULTCODE = 1;
   // private ValueCallback<Uri[]> mUploadMessageArr;
    private ValueCallback uploadMessage;

    static boolean ASWP_JSCRIPT     = true;

    // upload file from webview
    static boolean ASWP_FUPLOAD     = true;

    // enable upload from camera for photos
    static boolean ASWP_CAMUPLOAD   = true;

    // incase you want only camera files to upload
    static boolean ASWP_ONLYCAM	     = false;

    // upload multiple files in webview
    static boolean ASWP_MULFILE     = true;

    // track GPS locations
    static boolean ASWP_LOCATION    = true;

    // show ratings dialog; auto configured
    // edit method get_rating() for customizations
    static boolean ASWP_RATINGS     = true;

    // pull refresh current url
    static boolean ASWP_PULLFRESH	 = true;

    // show progress bar in app
    static boolean ASWP_PBAR        = true;

    // zoom control for webpages view
    static boolean ASWP_ZOOM        = false;

    // save form cache and auto-fill information
    static boolean ASWP_SFORM       = false;

    // whether the loading webpages are offline or online
    static boolean ASWP_OFFLINE     = false;

    // open external url with default browser instead of app webview
    static boolean ASWP_EXTURL      = true;


    /* -- SECURITY VARIABLES -- */

    // verify whether HTTPS port needs certificate verification
    static boolean ASWP_CERT_VERIFICATION = true;


    /* -- CONFIG VARIABLES -- */

    //complete URL of your website or offline webpage
    //static String ASWV_URL          = "file:///android_asset/offline.html";

    static String ASWV_URL          = "http://www.tractiletechnologies.com/RaithuKart/index.html";
    //static String ASWV_URL          = "http://192.168.0.104:8080/RaithuKart/index.html";


    //to upload any file type using "*/*"; check file type references for more
    static String ASWV_F_TYPE       = "*/*";


    /* -- RATING SYSTEM VARIABLES -- */

    static int ASWR_DAYS            = 3;	// after how many days of usage would you like to show the dialoge
    static int ASWR_TIMES           = 10;  // overall request launch times being ignored
    static int ASWR_INTERVAL        = 2;   // reminding users to rate after days interval


    private String asw_cam_message;
    private ValueCallback<Uri> asw_file_message;
    private ValueCallback<Uri[]> asw_file_path;
    private final static int asw_file_req = 1;

    private final static int loc_perm = 1;
    private final static int file_perm = 2;

    private SecureRandom random = new SecureRandom();

    private static final String TAG = MainActivity.class.getSimpleName();

    private Context mContext=MainActivity.this;

    private static final int REQUEST = 112;

    // contact variables
    private String codeFormat,codeContent;
    private String refrence = null;

    static String uri1="";
    static String finalfileName="";
    static String downloadBillText="";
    static String toNumber = "";
    public String billContent="";



    private UsbManager mUsbManager;
    private UsbDevice mDevice;
    private UsbDeviceConnection mConnection;
    private UsbInterface mInterface;
    private UsbEndpoint mEndPoint;
    private PendingIntent mPermissionIntent;
    EditText ed_txt;
    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    private static Boolean forceCLaim = true;

    HashMap<String, UsbDevice> mDeviceList;
    Iterator<UsbDevice> mDeviceIterator;
    byte[] testBytes;


    @SuppressLint("JavascriptInterface")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.mywebview = (WebView) findViewById(R.id.webView);
        //mywebview.loadUrl("http://www.tractiletechnologies.com/RaithuKart/index.html");
        mywebview.loadUrl(ASWV_URL);
        if (savedInstanceState == null)
        {
            //mywebview.loadUrl("http://www.tractiletechnologies.com/RaithuKart/index.html");
            mywebview.loadUrl(ASWV_URL);

        }
        mywebview.getSettings().setDomStorageEnabled(true);
        mywebview.getSettings().setSaveFormData(true);
        mywebview.getSettings().setAllowContentAccess(true);
        mywebview.getSettings().setAllowFileAccess(true);
        mywebview.getSettings().setSupportZoom(true);
        mywebview.setWebViewClient(new WebViewClient());
        mywebview.setClickable(true);
        mywebview.setWebChromeClient(new WebChromeClient());
        CookieManager.getInstance().setAcceptCookie(true);
        //mywebview.clearCache(true);



        mywebview.getSettings().setPluginState(WebSettings.PluginState.ON);
        mywebview.getSettings().setJavaScriptEnabled(true);
        mywebview.setBackgroundColor(Color.TRANSPARENT);
        mywebview.setBackgroundResource(R.drawable.tractilelogo);
        mywebview.addJavascriptInterface(new WebViewJavaScriptInterface(this),"app");
        get_file();
        if(!check_permission(4))
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_CONTACTS,Manifest.permission.WRITE_CONTACTS},1);

        //Load USB is for USB printing service for ESCPOS commands
        //loadUSB();
        //printUsb();


        if (savedInstanceState != null)
            ((WebView)findViewById(R.id.webView)).restoreState(savedInstanceState);
        mywebview.setWebViewClient(new WebViewClient(){

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                Log.e("tag","url overrride url  = "+ url);
                if(url.startsWith("whatsapp://")){

                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());
                    Uri uri=Uri.parse(url);
                    String msg = uri.getQueryParameter("text");
                    String[] splited = msg.trim().split("\\s+");
                    String function = uri.getQueryParameter("function");
                    String customerName = splited[1].replaceAll(",","");
                    String transactionDate = splited[8].replaceAll(",","");
                    final String cusName = uri.getQueryParameter("name");
                    String phone = uri.getQueryParameter("phone");
                    final String ru = uri.getQueryParameter("ru");
                    final String ru2 = uri.getQueryParameter("ru2");
                    final String action = uri.getQueryParameter("action");

                     toNumber = phone; // contains spaces.
                    toNumber = toNumber.replace("+", "").replace(" ", "");
                    finalfileName="invoice.PDF";
                    downloadBillText = "bill.txt";
                    if(!check_permission(2)){
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, file_perm);
                    }else {
                     //  DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));

                        //new Download_PDF(mContext,ru,finalfileName).execute();
                        //new Download_PDF(mContext,"D:/CustomImages/47/PDF/135/1252",finalfileName).execute();
                        if(ru != null && ru.trim().length() > 0)
                        {

                            if("print".equalsIgnoreCase(action)) {
                                new Download_PDF(mContext, ru2, downloadBillText).execute();
                            }else
                            {
                                new Download_PDF(mContext,ru,finalfileName).execute();
/*                                boolean output;
                                String number = toNumber.replace("91", "");
                                output = contactExists(mContext,number);

                                if(output){}else {

                                    String DisplayName = cusName;
                                    String MobileNumber = number;
                                    //String HomeNumber = "1111";
                                    //String WorkNumber = "2222";
                                    //String emailID = "email@nomail.com";
                                    //String company = "bad";
                                    //String jobTitle = "abcd";

                                    ArrayList < ContentProviderOperation > ops = new ArrayList < ContentProviderOperation > ();

                                    ops.add(ContentProviderOperation.newInsert(
                                            ContactsContract.RawContacts.CONTENT_URI)
                                            .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                                            .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                                            .build());

                                    //------------------------------------------------------ Names
                                    if (DisplayName != null) {
                                        ops.add(ContentProviderOperation.newInsert(
                                                ContactsContract.Data.CONTENT_URI)
                                                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                                                .withValue(ContactsContract.Data.MIMETYPE,
                                                        ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                                                .withValue(
                                                        ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
                                                        DisplayName).build());
                                    }

                                    //------------------------------------------------------ Mobile Number
                                    if (MobileNumber != null) {
                                        ops.add(ContentProviderOperation.
                                                newInsert(ContactsContract.Data.CONTENT_URI)
                                                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                                                .withValue(ContactsContract.Data.MIMETYPE,
                                                        ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                                                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, MobileNumber)
                                                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                                                        ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                                                .build());
                                    }

                                    //------------------------------------------------------ Home Numbers
                                       *//* if (HomeNumber != null) {
                                            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                                                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                                                    .withValue(ContactsContract.Data.MIMETYPE,
                                                            ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                                                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, HomeNumber)
                                                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                                                            ContactsContract.CommonDataKinds.Phone.TYPE_HOME)
                                                    .build());
                                        }

                                        //------------------------------------------------------ Work Numbers
                                        if (WorkNumber != null) {
                                            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                                                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                                                    .withValue(ContactsContract.Data.MIMETYPE,
                                                            ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                                                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, WorkNumber)
                                                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                                                            ContactsContract.CommonDataKinds.Phone.TYPE_WORK)
                                                    .build());
                                        }

                                        //------------------------------------------------------ Email
                                        if (emailID != null) {
                                            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                                                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                                                    .withValue(ContactsContract.Data.MIMETYPE,
                                                            ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                                                    .withValue(ContactsContract.CommonDataKinds.Email.DATA, emailID)
                                                    .withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
                                                    .build());
                                        }

                                        //------------------------------------------------------ Organization
                                        if (!company.equals("") && !jobTitle.equals("")) {
                                            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                                                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                                                    .withValue(ContactsContract.Data.MIMETYPE,
                                                            ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)
                                                    .withValue(ContactsContract.CommonDataKinds.Organization.COMPANY, company)
                                                    .withValue(ContactsContract.CommonDataKinds.Organization.TYPE, ContactsContract.CommonDataKinds.Organization.TYPE_WORK)
                                                    .withValue(ContactsContract.CommonDataKinds.Organization.TITLE, jobTitle)
                                                    .withValue(ContactsContract.CommonDataKinds.Organization.TYPE, ContactsContract.CommonDataKinds.Organization.TYPE_WORK)
                                                    .build());
                                        }*//*

                                    // Asking the Contact provider to create a new contact
                                    try {
                                        getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Toast.makeText(mContext, "Exception: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }*/


                            }



                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                public void run() {
                                    if("print".equalsIgnoreCase(action))
                                    {
                                        new Download_PDF(mContext,ru2,downloadBillText).execute();
                                        try {
                                            billContent = FileUtils.readFileToString(new File(mContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)+"/"+downloadBillText), StandardCharsets.UTF_8);
                                            printUsb();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }

                                    }else{

                                        new Download_PDF(mContext,ru,finalfileName).execute();


                                    Intent initaiteIntent = new Intent("android.intent.action.MAIN");
                                    initaiteIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                                    initaiteIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                    initaiteIntent.putExtra("jid", toNumber + "@s.whatsapp.net");
                                    initaiteIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(toNumber) + "@s.whatsapp.net");
                                    Intent sendIntent = new Intent("android.intent.action.MAIN");
                                    //sendIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                    sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(toNumber) + "@s.whatsapp.net");
                                    //sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    sendIntent.setAction(Intent.ACTION_SEND);
                                    sendIntent.setType("application/pdf");
                                    sendIntent.setPackage("com.whatsapp");
                                    //Uri imageUri = Uri.parse("file:///storage/emulated/0/Download/"+finalfileName);
                                    Uri imageUri = Uri.parse(mContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)+"/"+finalfileName);
                                    sendIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
                                    //startActivity(Intent.createChooser(sendIntent, "Share via"));
                                    startActivity(sendIntent);
                                    //finish();
                                    }
                                   // doPrint(mContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)+"/"+finalfileName);
                                   // genPdfFile();

                                    //print(mConnection, mInterface,mContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)+"/"+finalfileName);
                                }
                            }, 500);


                        }else
                        {
                            Intent initaiteIntent = new Intent("android.intent.action.MAIN");
                            initaiteIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                            initaiteIntent.putExtra("jid", toNumber + "@s.whatsapp.net");
                            Intent waIntent = new Intent("android.intent.action.MAIN");
                            waIntent.setAction(Intent.ACTION_SEND);
                            waIntent.putExtra("jid", toNumber + "@s.whatsapp.net");
                            waIntent.setType("text/plain");
                            waIntent.setPackage("com.whatsapp");
                            waIntent.putExtra(Intent.EXTRA_TEXT, URLDecoder.decode(msg));
                            startActivity(waIntent);
                        }
                        return true;
                    }
                }
                return false;
            }

            @Override
            public void onPageFinished(WebView mywebview, String ASWV_URL) {
                retriveDetails();
            }


            public boolean onKeyDown(int keyCode, KeyEvent event) {
                Log.i("TAG", ""+ keyCode);
                mywebview.loadUrl("javascript:scans('"+ keyCode +"')");
                //I think you'll have to manually check for the digits and do what you want with them.
                //Perhaps store them in a String until an Enter event comes in (barcode scanners i've used can be configured to send an enter keystroke after the code)
                return true;
            }

            /*public boolean contactExists(Context context, String number) {
                if (number != null) {
                    ContentResolver cr = context.getContentResolver();
                    Cursor curContacts = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);

                    while (curContacts.moveToNext()) {
                        String contactNumber = curContacts.getString(curContacts.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        contactNumber = contactNumber.replace("+91", "").replace(" ", "");
                        if (number.equals(contactNumber)) {
                            return true;
                        }
                    }
                    return false;
                } else {
                    return false;
                }
            }*/




        });



        mywebview.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {

                if(!check_permission(2)){
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, file_perm);
                }else {
                    DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                    request.setMimeType(mimeType);
                    String cookies = CookieManager.getInstance().getCookie(url);
                    request.addRequestHeader("cookie", cookies);
                    request.addRequestHeader("User-Agent", userAgent);
                    request.setDescription("Downloading file…");
                    request.setTitle(URLUtil.guessFileName(url, contentDisposition, mimeType));
                    request.allowScanningByMediaScanner();
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, URLUtil.guessFileName(url, contentDisposition, mimeType));
                    DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                    assert dm != null;
                    dm.enqueue(request);
                    Toast.makeText(getApplicationContext(), "Downloading file", Toast.LENGTH_LONG).show();
                }
            }
        });


        mywebview.setWebChromeClient(new WebChromeClient() {
            //Handling input[type="file"] requests for android API 16+
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture){
                if(ASWP_FUPLOAD) {
                    asw_file_message = uploadMsg;
                    Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                    i.addCategory(Intent.CATEGORY_OPENABLE);
                    i.setType(ASWV_F_TYPE);
                    if(ASWP_MULFILE) {
                        i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    }
                    startActivityForResult(Intent.createChooser(i, "File Chooser"), asw_file_req);
                }
            }
            //Handling input[type="file"] requests for android API 21+
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams){
                if(check_permission(2) && check_permission(3)) {
                    if (ASWP_FUPLOAD) {
                        if (asw_file_path != null) {
                            asw_file_path.onReceiveValue(null);
                        }
                        asw_file_path = filePathCallback;
                        Intent takePictureIntent = null;
                        if (ASWP_CAMUPLOAD) {
                            takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            if (takePictureIntent.resolveActivity(MainActivity.this.getPackageManager()) != null) {
                                File photoFile = null;
                                try {
                                    photoFile = create_image();
                                    takePictureIntent.putExtra("PhotoPath", asw_cam_message);
                                } catch (IOException ex) {
                                    Log.e(TAG, "Image file creation failed", ex);
                                }
                                if (photoFile != null) {
                                    asw_cam_message = "file:" + photoFile.getAbsolutePath();
                                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                                } else {
                                    takePictureIntent = null;
                                }
                            }
                        }
                        Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        if (!ASWP_ONLYCAM) {
                            contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
                            contentSelectionIntent.setType(ASWV_F_TYPE);
                            //contentSelectionIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            if (ASWP_MULFILE) {
                                contentSelectionIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            }
                        }
                        Intent[] intentArray;
                        if (takePictureIntent != null) {
                            intentArray = new Intent[]{takePictureIntent,pickPhoto};
                        } else {
                            intentArray = new Intent[0];
                        }
                        Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
                        chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
                        chooserIntent.putExtra(Intent.EXTRA_TITLE, "File Chooser");
                        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
                        startActivityForResult(chooserIntent, asw_file_req);
                    }
                    return true;
                }else{
                    get_file();
                    return false;
                }
            }

            //Getting webview rendering progress


            // overload the geoLocations permissions prompt to always allow instantly as app permission was granted previously
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                if(Build.VERSION.SDK_INT < 23 || check_permission(1)){
                    // location permissions were granted previously so auto-approve
                    callback.invoke(origin, true, false);
                } else {
                    // location permissions not granted so request them
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, loc_perm);
                }
            }
        });

        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        NetworkChangeReceiver networkChangeReceiver = new NetworkChangeReceiver();
        registerReceiver(networkChangeReceiver, intentFilter);
        networkChangeReceiver.setConnectionChangeCallback(this);
    }




    public void get_file(){
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

        //Checking for storage permission to write images for upload
        if (ASWP_FUPLOAD && ASWP_CAMUPLOAD && !check_permission(2) && !check_permission(3)) {
            ActivityCompat.requestPermissions(MainActivity.this, perms, file_perm);

            //Checking for WRITE_EXTERNAL_STORAGE permission
        } else if (ASWP_FUPLOAD && !check_permission(2)) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, file_perm);

            //Checking for CAMERA permissions
        } else if (ASWP_CAMUPLOAD && !check_permission(3)) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, file_perm);
        }
    }



    public boolean check_permission(int permission){
        switch(permission){
            case 1:
                return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

            case 2:
                return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;

            case 3:
                return ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;

            case 4:
                return ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED;




        }
        return false;
    }

    //Creating image file for upload
    private File create_image() throws IOException {
        @SuppressLint("SimpleDateFormat")
        String file_name    = new SimpleDateFormat("yyyy_mm_ss").format(new Date());
        String new_name     = "file_"+file_name+"_";
        File sd_directory   = mContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
                //Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(new_name, ".jpg", sd_directory);
    }
    @Override
    public void onConnectionChange(boolean isConnected) {
        if(isConnected){
            // will be called when internet is back
            //mywebview.loadUrl(ASWV_URL);
            String webUrl = mywebview.getUrl();
            //String url = "http://example.com/api_callback#access_token=XXXXXXXXXXXXXXX&state=enabled&scope=profile%20booking&token_type=bearer&expires_in=15551999";
            URL myurl;

            try {
                myurl = new URL(webUrl);
                refrence = myurl.getRef();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            if(refrence == null || refrence == "" )

                //mywebview.loadUrl("http://www.tractiletechnologies.com/RaithuKart/index.html");
                mywebview.loadUrl(ASWV_URL);

            else{}
        }
        else {
            mywebview.setBackgroundResource(R.drawable.connectionlogo);
            AlertDialog.Builder builder =new AlertDialog.Builder(this);
            builder.setTitle("No internet Connection");
            builder.setMessage("Please turn on internet connection to continue");
            builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        String value;
        if(requestCode == 49374){
            IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
            if (scanningResult.getFormatName() != null) {
                //we have a result
                codeContent = scanningResult.getContents();
                codeFormat = scanningResult.getFormatName();
                value = codeContent;
                mywebview.loadUrl("javascript:scans('"+ value +"')");
                // display it on screen
                /* formatTxt.setText("FORMAT: " + codeFormat +"");
                contentTxt.setText("CONTENT: " + codeContent);*/
            } else {
                mywebview.loadUrl("javascript:scans(0)");
            }
        }
        else if(requestCode == 1)
        {
            if (Build.VERSION.SDK_INT >= 21) {
            //getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            //getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
            Uri[] results = null;
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == asw_file_req) {
                    if (null == asw_file_path) {
                        return;
                    }
                    if (intent == null || intent.getData() == null) {
                        if (asw_cam_message != null) {
                            results = new Uri[]{Uri.parse(asw_cam_message)};
                        }
                    } else {
                        String dataString = intent.getDataString();
                        if (dataString != null) {
                            results = new Uri[]{ Uri.parse(dataString) };
                        } else {
                            if(ASWP_MULFILE) {
                                if (intent.getClipData() != null) {
                                    final int numSelectedFiles = intent.getClipData().getItemCount();
                                    results = new Uri[numSelectedFiles];
                                    for (int i = 0; i < numSelectedFiles; i++) {
                                        results[i] = intent.getClipData().getItemAt(i).getUri();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            asw_file_path.onReceiveValue(results);
            asw_file_path = null;
        } else {
            if (requestCode == asw_file_req) {
                if (null == asw_file_message) return;
                Uri result = intent == null || resultCode != RESULT_OK ? null : intent.getData();
                asw_file_message.onReceiveValue(result);
                asw_file_message = null;
            }
        }
        }
        else {
            if (resultCode == RESULT_OK && requestCode == 4) {
                // Check for the request code, we might be usign multiple startActivityForReslut
                switch (requestCode) {
                    case RESULT_PICK_CONTACT1:
                        Cursor cursor = null;
                        try {
                            String phoneNo = null ;
                            String name = null;
                            String[] array= new String[2];
                            Uri uri = intent.getData();
                            cursor = getContentResolver().query(uri, null, null, null, null);
                            cursor.moveToFirst();
                            int  phoneIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                            int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                            phoneNo = cursor.getString(phoneIndex);
                            name = cursor.getString(nameIndex);
                            mywebview.loadUrl("javascript:contact('"+ name + "&" + phoneNo +"')");
                            //formatTxt.setText("FORMAT: " + name +"    ");
                            // textView2.setText(phoneNo);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                }
            } else {
                Log.e("MainActivity", "Failed to pick contact");
            }
        }
    }

    //JavaScript interface for Native Communication b/w Webtechnogies and Android Intents
    private static final int RESULT_PICK_CONTACT1= 4;
    public String codeValue;
    public class WebViewJavaScriptInterface{

        Context context;
        public TextView TextView;
        public TextView formatTxt, contentTxt;
        public WebViewJavaScriptInterface(Context context){
            this.context = context;
        }

        //Add Contact
        @JavascriptInterface
        public void addContact() {
            Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
            startActivityForResult(contactPickerIntent, RESULT_PICK_CONTACT1);
        }
        //scan Codes
        @JavascriptInterface
        public void scanBarcode()
        {
            Log.d("myTag", "This is my message");
            Log.d("myTag", "BarCodeValue ");
            scanNow(TextView,"barcode");
        }

        //Remeber Me
        @JavascriptInterface
        public void remeberDetails(String userName,String password,String remeberValue) {
            saveUserDetails(userName,password,remeberValue);
        }
        @JavascriptInterface
        public void appendSavedDetails(){
            retriveDetails();
        }

        //Generate Excel File
        @JavascriptInterface
        public void generateExcelSheet(String shopName,String[] excelHeader,String excelData) throws ParseException {

            String path = getBaseContext().getFilesDir().getPath();
            WriteExcel test = new WriteExcel(shopName,excelHeader,excelData);
            File file = new File(getExternalFilesDir(null),shopName+".xls");
            test.setOutputFile(file);
            try {
                test.write();
                Toast.makeText(getApplicationContext(),"File downloaded ",Toast.LENGTH_LONG).show();
                File dir = new File(path);
                File newFile = new File(dir,shopName+".xls");
                DownloadManager downloadManager = (DownloadManager) context.getSystemService(DOWNLOAD_SERVICE);
                downloadManager.addCompletedDownload(newFile.getName(), newFile.getName(), true, "text/plain",file.getAbsolutePath(),file.length(),true);

            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(),"File not downloaded",Toast.LENGTH_LONG).show();
            } catch (WriteException | JSONException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(),"File not downloaded",Toast.LENGTH_LONG).show();
            }
        }

        @JavascriptInterface
        public boolean contactExists(String name,String number) {
            //context = context;
            if (number != null) {
                ContentResolver cr = context.getContentResolver();
                Cursor curContacts = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);

                while (curContacts.moveToNext()) {
                    String contactNumber = curContacts.getString(curContacts.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    contactNumber = contactNumber.replace("+91", "").replace(" ", "");
                    if (number.equals(contactNumber)) {
                        return true;
                    }
                }{
                    createContact(name,number);
                    return false;
                }

            } else {
                return false;
            }
        }





    }


    public void createContact(String cusName,String toNumber){
        boolean output;
        String number = toNumber.replace("91", "");
        //output = contactExists(mContext,number);



            String DisplayName = cusName;
            String MobileNumber = number;
            //String HomeNumber = "1111";
            //String WorkNumber = "2222";
            //String emailID = "email@nomail.com";
            //String company = "bad";
            //String jobTitle = "abcd";
        if(!check_permission(4))
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_CONTACTS,Manifest.permission.WRITE_CONTACTS},1);

        ArrayList < ContentProviderOperation > ops = new ArrayList < ContentProviderOperation > ();

            ops.add(ContentProviderOperation.newInsert(
                    ContactsContract.RawContacts.CONTENT_URI)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                    .build());

            //------------------------------------------------------ Names
            if (DisplayName != null) {
                ops.add(ContentProviderOperation.newInsert(
                        ContactsContract.Data.CONTENT_URI)
                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                        .withValue(ContactsContract.Data.MIMETYPE,
                                ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                        .withValue(
                                ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
                                DisplayName).build());
            }

            //------------------------------------------------------ Mobile Number
            if (MobileNumber != null) {
                ops.add(ContentProviderOperation.
                        newInsert(ContactsContract.Data.CONTENT_URI)
                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                        .withValue(ContactsContract.Data.MIMETYPE,
                                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                        .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, MobileNumber)
                        .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                                ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                        .build());
            }

            //------------------------------------------------------ Home Numbers
                                       /* if (HomeNumber != null) {
                                            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                                                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                                                    .withValue(ContactsContract.Data.MIMETYPE,
                                                            ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                                                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, HomeNumber)
                                                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                                                            ContactsContract.CommonDataKinds.Phone.TYPE_HOME)
                                                    .build());
                                        }

                                        //------------------------------------------------------ Work Numbers
                                        if (WorkNumber != null) {
                                            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                                                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                                                    .withValue(ContactsContract.Data.MIMETYPE,
                                                            ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                                                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, WorkNumber)
                                                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                                                            ContactsContract.CommonDataKinds.Phone.TYPE_WORK)
                                                    .build());
                                        }

                                        //------------------------------------------------------ Email
                                        if (emailID != null) {
                                            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                                                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                                                    .withValue(ContactsContract.Data.MIMETYPE,
                                                            ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                                                    .withValue(ContactsContract.CommonDataKinds.Email.DATA, emailID)
                                                    .withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
                                                    .build());
                                        }

                                        //------------------------------------------------------ Organization
                                        if (!company.equals("") && !jobTitle.equals("")) {
                                            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                                                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                                                    .withValue(ContactsContract.Data.MIMETYPE,
                                                            ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)
                                                    .withValue(ContactsContract.CommonDataKinds.Organization.COMPANY, company)
                                                    .withValue(ContactsContract.CommonDataKinds.Organization.TYPE, ContactsContract.CommonDataKinds.Organization.TYPE_WORK)
                                                    .withValue(ContactsContract.CommonDataKinds.Organization.TITLE, jobTitle)
                                                    .withValue(ContactsContract.CommonDataKinds.Organization.TYPE, ContactsContract.CommonDataKinds.Organization.TYPE_WORK)
                                                    .build());
                                        }*/

            // Asking the Contact provider to create a new contact
            try {
                getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(mContext, "Exception: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }



        }

























    public void saveUserDetails(String userName,String password,String remeberValue) {

        SharedPreferences sp=getSharedPreferences("Login", MODE_PRIVATE);
        SharedPreferences.Editor Ed=sp.edit();
        Ed.putString("Unm",userName);
        Ed.putString("Psw",password);
        Ed.putString("rem",remeberValue);
        Ed.commit();
    }

    public void retriveDetails() {

        SharedPreferences sp1=this.getSharedPreferences("Login", MODE_PRIVATE);
        String unm=sp1.getString("Unm", "");
        String pass = sp1.getString("Psw", "");
        String rem = sp1.getString("rem", null);
        mywebview.loadUrl("javascript:savedDetails('"+ unm +"/"+pass +"/"+ rem+"')");
    }

      public void scanNow(View view, String fun)
    {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
        integrator.setCaptureActivity(AnyOrientationCaptureActivity.class);
        integrator.setOrientationLocked(false);
        Log.d("myTag", "This is my message");
        // integrator.setPrompt(this.getString(R.string.scan_bar_code));
        // integrator.setResultDisplayDuration(0);
        // integrator.setWide(); // Wide scanning rectangle, may work better for 1D barcodes
        integrator.setCameraId(0); // Use a specific camera of the device
        integrator.initiateScan();
    }

    @Override
    public void onBackPressed() {

    }




    /*==============================================================================================
    ===========================================USB PART=============================================
    ==============================================================================================*/


    private final BroadcastReceiver usbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (com.tractile.smartmerchanterp.MainActivity.ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbManager usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
                    UsbDevice usbDevice = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if (usbManager != null && usbDevice != null) {
                            // printIt(new UsbConnection(usbManager, usbDevice));
                            new AsyncUsbEscPosPrint(context)
                                    .execute(getAsyncEscPosPrinter(new UsbConnection(usbManager, usbDevice)));
                        }
                    }
                }
            }
        }
    };

    public void printUsb() {
        UsbConnection usbConnection = UsbPrintersConnections.selectFirstConnected(this);
        UsbManager usbManager = (UsbManager) this.getSystemService(Context.USB_SERVICE);

        if (usbConnection == null || usbManager == null) {
            new AlertDialog.Builder(this)
                    .setTitle("USB Connection")
                    .setMessage("No USB printer found.")
                    .show();
            return;
        }

        PendingIntent permissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(com.tractile.smartmerchanterp.MainActivity.ACTION_USB_PERMISSION), 0);
        IntentFilter filter = new IntentFilter(com.tractile.smartmerchanterp.MainActivity.ACTION_USB_PERMISSION);
        registerReceiver(this.usbReceiver, filter);
        usbManager.requestPermission(usbConnection.getDevice(), permissionIntent);
    }

     /*==============================================================================================
    ===================================ESC/POS PRINTER PART=========================================
    ==============================================================================================*/


    /**
     * Synchronous printing
     */
    @SuppressLint("SimpleDateFormat")
    public void printIt(DeviceConnection printerConnection) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("'on' yyyy-MM-dd 'at' HH:mm:ss");
            EscPosPrinter printer = new EscPosPrinter(printerConnection, 203, 48f, 32);
            printer
                    .printFormattedText(
                            "[C]<img>" + PrinterTextParserImg.bitmapToHexadecimalString(printer, this.getApplicationContext().getResources().getDrawableForDensity(R.drawable.tractilelogo, DisplayMetrics.DENSITY_MEDIUM)) + "</img>\n" +
                                    "[L]\n" +
                                    "[C]<u><font size='big'>ORDER N°045</font></u>\n" +
                                    "[L]\n" +
                                    "[C]<u type='double'>" + format.format(new Date()) + "</u>\n" +
                                    "[C]\n" +
                                    "[C]================================\n" +
                                    "[L]\n" +
                                    "[L]<b>BEAUTIFUL SHIRT</b>[R]9.99€\n" +
                                    "[L]  + Size : S\n" +
                                    "[L]\n" +
                                    "[L]<b>AWESOME HAT</b>[R]24.99€\n" +
                                    "[L]  + Size : 57/58\n" +
                                    "[L]\n" +
                                    "[C]--------------------------------\n" +
                                    "[R]TOTAL PRICE :[R]34.98€\n" +
                                    "[R]TAX :[R]4.23€\n" +
                                    "[L]\n" +
                                    "[C]================================\n" +
                                    "[L]\n" +
                                    "[L]<u><font color='bg-black' size='tall'>Customer :</font></u>\n" +
                                    "[L]Raymond DUPONT\n" +
                                    "[L]5 rue des girafes\n" +
                                    "[L]31547 PERPETES\n" +
                                    "[L]Tel : +33801201456\n" +
                                    "\n" +
                                    "[C]<barcode type='ean13' height='10'>831254784551</barcode>\n" +
                                    "[L]\n" +
                                    "[C]<qrcode size='20'>http://www.developpeur-web.dantsu.com/</qrcode>\n"
                    );
        } catch (EscPosConnectionException e) {
            e.printStackTrace();
            new AlertDialog.Builder(this)
                    .setTitle("Broken connection")
                    .setMessage(e.getMessage())
                    .show();
        } catch (EscPosParserException e) {
            e.printStackTrace();
            new AlertDialog.Builder(this)
                    .setTitle("Invalid formatted text")
                    .setMessage(e.getMessage())
                    .show();
        } catch (EscPosEncodingException e) {
            e.printStackTrace();
            new AlertDialog.Builder(this)
                    .setTitle("Bad selected encoding")
                    .setMessage(e.getMessage())
                    .show();
        } catch (EscPosBarcodeException e) {
            e.printStackTrace();
            new AlertDialog.Builder(this)
                    .setTitle("Invalid barcode")
                    .setMessage(e.getMessage())
                    .show();
        }
    }

    /**
     * Asynchronous printing
     */
    @SuppressLint("SimpleDateFormat")
    public AsyncEscPosPrinter getAsyncEscPosPrinter(DeviceConnection printerConnection) {
        SimpleDateFormat format = new SimpleDateFormat("'on' yyyy-MM-dd 'at' HH:mm:ss");
        AsyncEscPosPrinter printer = new AsyncEscPosPrinter(printerConnection, 203, 80f, 48);
        return printer.setTextToPrint(billContent);
    }



  /*@RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void doPrint(String fileName) {
        // Get a PrintManager instance

        // Set job name, which will be displayed in the print queue

        PrintManager printManager = null;

        // Start a print job, passing in a PrintDocumentAdapter implementation
        // to handle the generation of a print document
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //printManager.print(jobName, new PdfDocumentAdapter(MainActivity.this,fileName),
                    //null);
            printManager = (PrintManager) MainActivity.this
                    .getSystemService(Context.PRINT_SERVICE);
            String jobName = MainActivity.this.getString(R.string.app_name) + " Document";
            printManager.print(jobName, new PdfDocumentAdapter(MainActivity.this,fileName),
                    null);

        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void genPdfFile() {
        PrintManager printManager = (PrintManager) getSystemService(Context.PRINT_SERVICE);

        // Get a print adapter instance
        PrintDocumentAdapter printAdapter;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            printAdapter = mywebview.createPrintDocumentAdapter("my.pdf");
        } else {
            printAdapter = mywebview.createPrintDocumentAdapter();
        }

        // Create a print job with name and adapter instance
        String jobName = getString(R.string.app_name) + " Document";

        PrintAttributes attributes = new PrintAttributes.Builder()
                .setMediaSize(PrintAttributes.MediaSize.ISO_A4)
                .setResolution(new PrintAttributes.Resolution("id", Context.PRINT_SERVICE, 200, 200))
                .setColorMode(PrintAttributes.COLOR_MODE_COLOR)
                .setMinMargins(PrintAttributes.Margins.NO_MARGINS)
                .build();

        try {
            printManager.print(jobName, printAdapter, attributes);
        }catch (Exception e) {
            e.printStackTrace();
        }


    }*/


   /* public void loadUSB()
    {
        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        mDeviceList = mUsbManager.getDeviceList();

        if (mDeviceList.size() > 0) {
            mDeviceIterator = mDeviceList.values().iterator();

            Toast.makeText(this, "Device List Size: " + String.valueOf(mDeviceList.size()), Toast.LENGTH_SHORT).show();

           // TextView textView = (TextView) findViewById(R.id.usbDevice);
            String usbDevice = "";
            while (mDeviceIterator.hasNext()) {
                UsbDevice usbDevice1 = mDeviceIterator.next();
                usbDevice += "\n" +
                        "DeviceID: " + usbDevice1.getDeviceId() + "\n" +
                        "DeviceName: " + usbDevice1.getDeviceName() + "\n" +
                        "Protocol: " + usbDevice1.getDeviceProtocol() + "\n" +
                       *//* "Product Name: " + usbDevice1.getProductName() + "\n" +
                        "Manufacturer Name: " + usbDevice1.getManufacturerName() + "\n" +*//*
                        "DeviceClass: " + usbDevice1.getDeviceClass() + " - " + translateDeviceClass(usbDevice1.getDeviceClass()) + "\n" +
                        "DeviceSubClass: " + usbDevice1.getDeviceSubclass() + "\n" +
                        "VendorID: " + usbDevice1.getVendorId() + "\n" +
                        "ProductID: " + usbDevice1.getProductId() + "\n";

                int interfaceCount = usbDevice1.getInterfaceCount();
                Toast.makeText(this, "INTERFACE COUNT: " + String.valueOf(interfaceCount), Toast.LENGTH_SHORT).show();

                mDevice = usbDevice1;

                Toast.makeText(this, "Device is attached", Toast.LENGTH_SHORT).show();
                break;
                //textView.setText(usbDevice);
            }

            mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
            IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
            registerReceiver(mUsbReceiver, filter);

            mUsbManager.requestPermission(mDevice, mPermissionIntent);



        } else {
            Toast.makeText(this, "Please attach printer via USB", Toast.LENGTH_SHORT).show();
        }
    }


            private void print(final UsbDeviceConnection connection, final UsbInterface usbInterface, String fileName) {
         String test = "Hello" + "\n\n";
                test +=  "\n\n"+"I'm Center"+"\n";
                test +=  "I'm New Line";
        //testBytes = test.getBytes();


                testBytes = test.getBytes();


        if (usbInterface == null) {
            Toast.makeText(this, "INTERFACE IS NULL", Toast.LENGTH_SHORT).show();
        } else if (connection == null) {
            Toast.makeText(this, "CONNECTION IS NULL", Toast.LENGTH_SHORT).show();
        } else if (forceCLaim == null) {
            Toast.makeText(this, "FORCE CLAIM IS NULL", Toast.LENGTH_SHORT).show();
        } else {

            connection.claimInterface(usbInterface, forceCLaim);

            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {

                    byte[] cut_paper = {0x1D, 0x56, 0x41, 0x10};
                    connection.bulkTransfer(mEndPoint, testBytes, testBytes.length, 0);

                   // connection.bulkTransfer(mEndPoint, cut_paper, cut_paper.length, 0);
                }
            });
            thread.run();
        }
    }*/


    /*final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if (device != null) {
                            //call method to set up device communication
                            mInterface = device.getInterface(0);
                            mEndPoint = mInterface.getEndpoint(1);// 0 IN and  1 OUT to printer.
                            mConnection = mUsbManager.openDevice(device);

                        }
                    } else {
                        Toast.makeText(context, "PERMISSION DENIED FOR THIS DEVICE", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    };*/


    /*private String translateDeviceClass(int deviceClass) {

        switch (deviceClass) {

            case UsbConstants.USB_CLASS_APP_SPEC:
                return "Application specific USB class";

            case UsbConstants.USB_CLASS_AUDIO:
                return "USB class for audio devices";

            case UsbConstants.USB_CLASS_CDC_DATA:
                return "USB class for CDC devices (communications device class)";

            case UsbConstants.USB_CLASS_COMM:
                return "USB class for communication devices";

            case UsbConstants.USB_CLASS_CONTENT_SEC:
                return "USB class for content security devices";

            case UsbConstants.USB_CLASS_CSCID:
                return "USB class for content smart card devices";

            case UsbConstants.USB_CLASS_HID:
                return "USB class for human interface devices (for example, mice and keyboards)";

            case UsbConstants.USB_CLASS_HUB:
                return "USB class for USB hubs";

            case UsbConstants.USB_CLASS_MASS_STORAGE:
                return "USB class for mass storage devices";

            case UsbConstants.USB_CLASS_MISC:
                return "USB class for wireless miscellaneous devices";

            case UsbConstants.USB_CLASS_PER_INTERFACE:
                return "USB class indicating that the class is determined on a per-interface basis";

            case UsbConstants.USB_CLASS_PHYSICA:
                return "USB class for physical devices";

            case UsbConstants.USB_CLASS_PRINTER:
                return "USB class for printers";

            case UsbConstants.USB_CLASS_STILL_IMAGE:
                return "USB class for still image devices (digital cameras)";

            case UsbConstants.USB_CLASS_VENDOR_SPEC:
                return "Vendor specific USB class";

            case UsbConstants.USB_CLASS_VIDEO:
                return "USB class for video devices";

            case UsbConstants.USB_CLASS_WIRELESS_CONTROLLER:
                return "USB class for wireless controller devices";

            default:
                return "Unknown USB class!";
        }
    }*/



}
