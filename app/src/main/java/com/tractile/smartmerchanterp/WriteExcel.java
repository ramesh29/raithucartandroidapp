package com.tractile.smartmerchanterp;

import org.json.*;
import org.json.simple.parser.ParseException;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.UnderlineStyle;
import jxl.write.Formula;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;


public class WriteExcel {

    private WritableCellFormat timesBoldUnderline;
    private WritableCellFormat times;
    private File file;
    private String shopNa;
    private String[] excelHe;
    private String excelDa;

    public WriteExcel(String shopName, String[] excelHeader, String excelData) throws ParseException {
        shopNa    = shopName;
        excelHe   = excelHeader;
        excelDa   = excelData;
    }

    public void setOutputFile(File inputFile) {
        this.file = inputFile;
    }

    public void write() throws IOException, WriteException, ParseException, JSONException {

       /* File file = new File(inputFile);
        File file = new File(getExternalFilesDir(null),"plik.xls");
        System.out.println(excelDa.length);*/
        WorkbookSettings wbSettings = new WorkbookSettings();
        wbSettings.setLocale(new Locale("en", "EN"));
        WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
        workbook.createSheet("Item Details", 0);
        WritableSheet excelSheet = workbook.getSheet(0);
        createLabel(excelSheet);
        createContent(excelSheet);
        workbook.write();
        workbook.close();
    }

    private void createLabel(WritableSheet sheet) throws WriteException {

        // Lets create a times font
        WritableFont times10pt = new WritableFont(WritableFont.TIMES, 10);
        // Define the cell format
        times = new WritableCellFormat(times10pt);
        // Lets automatically wrap the cells
        times.setWrap(true);
        // create create a bold font with unterlines
        WritableFont times10ptBoldUnderline = new WritableFont(
                WritableFont.TIMES, 10, WritableFont.BOLD, false,
                UnderlineStyle.NO_UNDERLINE);
        timesBoldUnderline = new WritableCellFormat(times10ptBoldUnderline);
        // Lets automatically wrap the cells
        //timesBoldUnderline.setWrap(true);

      /*  CellView cv = new CellView();
        cv.setFormat(times);
        //cv.setFormat(timesBoldUnderline);
        //cv.setSize(4);
        cv.setSize(25 * 256);*/

        // Write a few headers
        for(int i =0;i<excelHe.length;i++) {
            int widthInChars = 23;
            sheet.setColumnView(i, widthInChars);
            addCaption(sheet, i, 0, excelHe[i]);
        }
    }

    private void createContent(WritableSheet sheet) throws WriteException, RowsExceededException, ParseException, JSONException {

        // Write a few number
        int rows;
        try
        {
            JSONArray jsonArray = new JSONArray(excelDa);
            rows = jsonArray.length();
            for(int i=1;i<=rows;i++)
            {
                int col = 0;
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                addDetails(sheet, col, i, jsonObject1.optString("itemName"));
                addDetails(sheet, ++col, i, jsonObject1.optString("itemDesc"));
                addDetails(sheet, ++col, i, jsonObject1.optString("itemUOM"));
                addDetails(sheet, ++col, i, jsonObject1.optString("totalQuantity"));
                addDetails(sheet, ++col, i, jsonObject1.optString("itemPurchaseCost"));
                addDetails(sheet, ++col, i, jsonObject1.optString("itemSoldCost"));
                //addDetails(sheet, ++col, i, jsonObject1.optString("lowQuantityValue"));
                addDetails(sheet, ++col, i, jsonObject1.optString("stockValue"));
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private void addCaption(WritableSheet sheet, int column, int row, String s)
            throws RowsExceededException, WriteException {
        Label label;
        label = new Label(column, row, s, timesBoldUnderline);
        sheet.addCell(label);
    }

   /* private void addNumber(WritableSheet sheet, int column, int row,
                           Integer integer) throws WriteException, RowsExceededException {
        Number number;
        number = new Number(column, row, integer, times);
        sheet.addCell(number);
    }*/

    private void addDetails(WritableSheet sheet, int column, int row,
                           String integer) throws WriteException, RowsExceededException {
        Label label;
        label = new Label(column, row, integer, times);
        sheet.addCell(label);
    }

    private void addLabel(WritableSheet sheet, int column, int row, String s)
            throws WriteException, RowsExceededException {
        Label label;
        label = new Label(column, row, s, times);
        sheet.addCell(label);
    }

   /* public static void main(String[] args) throws WriteException, IOException {
        WriteExcel test = new WriteExcel();
        test.setOutputFile("F:/RAMESH/TRACTILE_OFFICE_2062020/lars.xls");
        test.write();
        System.out
                .println("Please check the result file under c:/temp/lars.xls ");
    }*/
}