package com.pos.escposprinter.exceptions;

public class EscPosBarcodeException extends Exception {
    public EscPosBarcodeException(String errorMessage) {
        super(errorMessage);
    }
}
