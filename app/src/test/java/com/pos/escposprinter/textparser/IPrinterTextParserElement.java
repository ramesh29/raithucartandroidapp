package com.pos.escposprinter.textparser;

import com.pos.escposprinter.EscPosPrinterCommands;
import com.pos.escposprinter.exceptions.EscPosEncodingException;

public interface IPrinterTextParserElement {
    int length() throws EscPosEncodingException;
    IPrinterTextParserElement print(EscPosPrinterCommands printerSocket) throws EscPosEncodingException;
}
